//
//  BST.cpp
//

#include "BST.h"
#include "Flags.h"

/* **************************************************************** */

#if ALL || CONSTRUCTOR
template <class T>
BST<T>::BST(){
	root = NULL;
  	numNodes = 0;
}
#endif

/* **************************************************************** */

#if ALL || DESTRUCTOR
template <class T>
BST<T>::~BST(){
    makeEmpty();
	numNodes = 0;
}
#endif

/* **************************************************************** */

#if ALL || ISEMPTY
template <class T>
bool BST<T>::isEmpty(){
    if(root == NULL){
		return true;
	}
	return false;
}
#endif

/* **************************************************************** */

#if ALL || FIND
template <class T>
bool BST<T>::find(const T & x){
  if( findNode(root, x) == NULL ){
    return false;
  }else if( findNode(root, x)->data == x ){
    return true;
  }
  return false;
}
#endif

/* **************************************************************** */

#if ALL || FINDMIN
template <class T>
const T& BST<T>::findMin(){
	Node<T> * n = findMinNode(root);
	assert(n != NULL);
	return n->data;
}
#endif

/* **************************************************************** */

#if ALL || FINDMAX
template <class T>
const T& BST<T>::findMax(){
	Node<T> * n = findMaxNode(root);
	assert(n != NULL);
	return n->data;
}
#endif

/* **************************************************************** */

#if ALL || INSERT
template <class T>
void BST<T>::insert(const T & x){
  root = insertNode( root, x );
  numNodes++;
}
#endif

/* **************************************************************** */

#if ALL || REMOVE
template <class T>
void BST<T>::remove( const T & x ){

  Node<T> * curr;
  Node<T> * parent;
  //finds the parent of x
  //parent == node before curr
  parent = findParentOf( x );
  // finds node with the x value
  curr = findNode( root, x );

  //case 1: value not in tree or empty tree
  if( isEmpty() || !find( x ) ){

  }

  //case 2:
  //node with no children
  //leaf node

  else if( curr->left == NULL && curr->right == NULL ){
	//the node is root
	if(curr == root){
		root = NULL;
	}
    //makes parent left & right == NULL
    if( parent->left == curr ){
      parent->left = NULL;
    }else{
      parent->right = NULL;
    }
    //delete curr which should == x
    delete curr;
	numNodes--;
  }

  //case 3:
  //node with one child
  //replace node with its child

  else if( ( curr->left == NULL && curr->right != NULL ) || ( curr->left != NULL && curr->right == NULL ) ){
    //with right child, no left child
    if( curr->left == NULL && curr->right != NULL ){
      if(parent->left == curr){
        parent->left = curr->right;
        delete curr;
      }else{
        parent->right = curr->right;
        delete curr;
      }
    }
    else{ //with left child , no right child
      if(parent->left == curr){
        parent->left = curr->left;
        delete curr;
      }else{
        parent->right = curr->left;
        delete curr;
      }
    }
	numNodes--;
  }

  //case 4:
  //Node with 2 children

  // replace node with smallest value in right subtree
  else{
	  Node<T> * successor = findSuccessor( curr );
	  T temp = successor->data;
	  remove( successor->data );
	  curr->data = temp;
	numNodes--;
  }
}
#endif

/* **************************************************************** */

#if ALL || MAKEEMPTY
template <class T>
void BST<T>::makeEmpty(){
  removeAllNodes( root );
  root = NULL;
  numNodes = 0;
}
#endif

/* **************************************************************** */



/* **************************************************************** */
/*  Private methods                                                 */
/* **************************************************************** */
#if ALL || FINDNODE
template <class T>
Node<T> * BST<T>::findNode( Node<T> * node, const T & x ){
	if( node == NULL || x == node->data ){
		return node;
	}else if( x > node->data ){
		return findNode( node->right, x );
	}else if( x < node->data ){
		return findNode( node->left, x );
	}
return node;
}
#endif

/* **************************************************************** */

#if ALL || FINDMINNODE
template <class T>
Node<T> * BST<T>::findMinNode(Node<T> * node){
	if( node->left == NULL ){
		return node;
	}else{
		return findMinNode(node->left);
	}
}
#endif

/* **************************************************************** */

#if ALL || FINDMAXNODE
template <class T>
Node<T> * BST<T>::findMaxNode(Node<T> * node){
	if( node->right == NULL ){
		return node;
	}else{
		return findMaxNode(node->right);
	}
}
#endif

/* **************************************************************** */

#if ALL || INSERTNODE
template <class T>
Node<T> * BST<T>::insertNode(Node<T> * node, const T & x){
	if( node == NULL ){
		node = new Node<T>(x);
	}else if(x < node->data){
		node->left = insertNode(node->left, x);
	}else if( x > node->data){
		node->right = insertNode(node->right, x);
	}
	return node;
}
#endif

/* **************************************************************** */

#if ALL || FINDSUCCESSOR
template <class T>
Node<T> * BST<T>::findSuccessor(Node<T> * node){
  Node<T> * curr;
  curr = node->right;
  while(curr->left != NULL){
    curr = curr->left;
  }
  return curr;
}
#endif

/* **************************************************************** */

#if ALL || FINDPARENTOF
template <class T>
Node<T> * BST<T>::findParentOf(const T & x){
 //This Tree is empty! or x not in tree!
  if( isEmpty() || !find(x) ) {
    return NULL;
  }
  Node<T> * curr;
  Node<T> * parent;
  curr = root;
  parent = curr;

  while(curr != NULL){
    if(curr->data == x){
      break;
    }else{
      parent = curr;
      if(x > curr->data){
        curr = curr->right;
      }else{
        curr = curr->left;
      }
    }
  }
  return parent;
}
#endif

/* **************************************************************** */

#if ALL || REMOVEALLNODES
template <class T>
void BST<T>::removeAllNodes(Node<T> * node){
  if( node != NULL ){
    removeAllNodes(node->right);
    removeAllNodes(node->left);
    delete node;
  }
}
#endif

/* **************************************************************** */



/* **************************************************************** */
/* Do NOT modify anything below this line                           */
/* **************************************************************** */

#ifndef BUILD_LIB
// Print tree
template <class T>
void BST<T>::printTree(){
    if (!isEmpty())
    {
        printNodesInOrder(root);
        std::cout << std::endl;
    } else {
        std::cout << "Empty Tree" << std::endl;
    }
}

// Print tree using level order traversal
template <class T>
void BST<T>::printNodesInOrder(Node<T> * node)
{
    Node<T>*q[100];
    int head = 0;
    int tail = 0;
    q[0] = root;
    tail++;

    while (head != tail)
    {
        Node<T> *n = q[head];
        head++;
        std::cout << "Node " << n->data << " ";
        if (n->left != NULL)
        {
            std::cout << " left child: " << n->left->data;
            q[tail] = n->left;
            tail++;
        }
        if (n->right != NULL)
        {
            std::cout << " right child: " << n->right->data;
            q[tail] = n->right;
            tail++;
        }

        if (n->left == NULL && n->right == NULL)
        {
            std::cout << " no children";
        }
        std::cout << std::endl;
    }
}
#endif

template class BST<int>;
